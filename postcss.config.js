// https://github.com/postcss/postcss#articles

module.exports = {
    plugins: [
        require('autoprefixer')
    ]
};