import dispatcher from "../../dispatcher";
import { EventEmitter } from "../../utils";

const eventEmitter = new EventEmitter();
let posiiton = {};
let direction = {};

function _handleEvent (e) {
    if (e.type === 'mouse:move') {
        posiiton = e.position;
        direction = e.direction;
    }
}

function _init () {
    dispatcher.subscribe(_handleEvent);
}

function getData () {
    return {
        position: posiiton,
        direction: direction
    }
}

_init();

export default {
    eventEmitter: eventEmitter,
    getData: getData
}

