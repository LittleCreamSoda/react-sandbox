import domReady from '@mikaelkristiansson/domready';

//modules
import main from './react/main';
import mouse from './components/mouse/mouse.view';

domReady( () => {
    main.init();
    mouse.init();
});

