import { combineReducers } from "redux";
import { createStore } from 'redux';
import { articleReducer} from "./action-reducers/article-reducer";
import { imageReducer } from "./action-reducers/image-reducer";

const rootReducer = combineReducers({
    articleReducer: articleReducer,
    imageReducer: imageReducer
});

let store = createStore(rootReducer);

export default store;