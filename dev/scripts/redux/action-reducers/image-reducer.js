import { CHANGE_THE_IMAGE, START_THE_SCENE } from "../action-types";
import reactImage from 'react.png';

const initialState = {
    imageSrc: reactImage,
    iframe: ""
};

export const imageReducer = (state = initialState, action) => {
    if (action.type === CHANGE_THE_IMAGE) {
        return {
            imageSrc: action.src,
            iframe: action.iframe
        };
    } else if (action.type === START_THE_SCENE) {
        return {
            shouldSceneStart: true
        }
    }

    return state;
};