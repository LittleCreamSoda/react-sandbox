import { GO_TO_THE_RIVER } from "redux/action-types";

const initialState = {
    article: ""
};

export const articleReducer = (state = initialState, action) => {
    if (action.type === GO_TO_THE_RIVER) {
        return {
            article: action.article
        };
    }

    return state;
};