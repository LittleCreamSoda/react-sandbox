import { GO_TO_THE_RIVER, CHANGE_THE_IMAGE, START_THE_SCENE } from "./action-types";

export const enableArticle = (article) => {
    return {
        type: GO_TO_THE_RIVER,
        article: article
    }
};

export const changeImage = (src, iframe = "") => {
    return {
        type: CHANGE_THE_IMAGE,
        src: src,
        iframe: iframe
    }
};

export const startScene = () => {
  return {
      type: START_THE_SCENE
  }
};

