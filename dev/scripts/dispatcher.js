import { EventEmitter } from "utils";

const dispatcher = new EventEmitter();

export default dispatcher;