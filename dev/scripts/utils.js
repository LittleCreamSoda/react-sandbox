// export class Mouse {
//     constructor () {
//         this.position = {
//             x: 0,
//             y: 0
//         };
//         this.direction = {
//             x: 0,
//             y: 0
//         };
//     }
//
//     updatePosition (e) {
//         this.prevX = this.position.x;
//         this.prevY = this.position.y;
//
//         this.position.x = e.x;
//         this.position.y = e.y;
//
//         this.direction.x = Math.sign(this.position.x - this.prevX);
//         this.direction.y = Math.sign(this.position.y - this.prevY);
//     }
// }

export class EventEmitter {
    constructor() {
        this.handlers = new Set();
    }
    dispatch (e) {
        for (const handler of this.handlers) {
            handler(e);
        }
    };
    subscribe (handlers) {
        for (let i = 0; i < arguments.length; i++) {
            this.handlers.add(arguments[i]);
        }

    }
    unsubscribe (handlers) {
        for (let i = 0; i < arguments.length; i++) {
            this.handlers.delete(arguments[i]);
        }
    }
}

export function http(action, callback, data, body, method, headers) {
    // const controller = new AbortController();
    // const signal = controller.signal;

    let settings = {
        method: 'GET',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        body: null,
        mode: 'cors',
        cache: 'no-store'
        // signal
    };

    if (data) {
        settings.method = 'POST';
        settings.body = data;
    }

    if (headers && typeof headers === 'object') {
        for (let key in headers) {
            if (headers.hasOwnProperty(key)) settings.headers[key] = headers[key];
        }
    }

    if (method) settings.method = method;

    if (settings.method !== 'POST') {
        settings.headers['Content-type'] = 'application/x-www-form-urlencoded; charset=UTF-8';
    }


    fetch(action, settings)
        .then(function (response) {
            if (body === 'blob') {
                return response.blob();
            } else if (body === 'text') {
                return response.text();
            } else {
                return response.json();
            }
        })
        .then(function (response) {
            if (callback) callback(response, true);
        })
        .catch(function (error) {
            console.warn(error);
            if (callback) callback(error, false);
        });

    // return controller;
}

export function createMatrix(angle) {
    return new Float32Array([
        Math.cos(angle), Math.sin(angle),
        -Math.sin(angle), Math.cos(angle)
    ]);
}

export function getDotProduct(x, y) {
    if (x.length !== y.length) return;

    let dotProduct = 0;

    for (let element in x) {
        dotProduct += x[element] * y[element];
    }

    return dotProduct
}


