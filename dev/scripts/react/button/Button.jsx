import React from "react";
import { connect } from "react-redux";

import { enableArticle } from "redux/action-creators";
import { http } from "../../utils";

class ButtonComponent extends React.Component {
    element = React.createRef();

    handleResponse = (response) => {
        if (response.status !== "success") return;

        return response.article;
    };

    handleClick = () => {
        const action = this.props.dataAction;
        let data = new FormData();

        data.set("articleType", "идущий к реке");

        setTimeout( () => {
            http(action, (response) => {
                this.props.enableArticle(this.handleResponse(response));
                this.setState({
                    articleLoaded: true
                });
            });
        }, 1000);

        this.element.current.classList.add('enable-article--clicked');
    };

    render() {
        return (
            <button ref={this.element} className="enable-article" onClick={this.handleClick }>Преисполниться в сознании</button>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        enableArticle: (articleType) => {
            dispatch( enableArticle(articleType) );
        }
    }
};

const Button = connect(null, mapDispatchToProps)(ButtonComponent);

export default Button;
