import React from 'react';

export const ThemeContext = React.createContext({ theme: "light" });

class ThemeProvider extends React.Component {
    changeTheme = (newTheme) => {
      this.setState( {
          theme: newTheme
      });
    };

    state = {
        theme: 'dark',
        changeTheme: this.changeTheme
    };

    render() {
        return (
            <ThemeContext.Provider value={this.state}>
                {this.props.children}
            </ThemeContext.Provider>
        );
    }
}

export default ThemeProvider;
