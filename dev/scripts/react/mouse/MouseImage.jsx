import React from "react";
import TweenMax from "gsap";
import { connect } from "react-redux";
import { startScene } from "../../redux/action-creators";

class MouseImageComponent extends React.Component {
    element = React.createRef();
    iframe = React.createRef();

    state = {
        iframe: null,
        iframeLoaded: false
    };

    componentDidUpdate() {
        TweenMax.to(this.element.current, 0.6, {
            ease: Power3.ease,
            x: this.props.mouseData.position.x,
            y: this.props.mouseData.position.y
        });

        if (this.props.iframe && !this.state.iframe) {
            this.setState({
                iframe: this.props.iframe
            });
        }

        if (this.iframe.current && !this.state.iframeLoaded) {
            this.iframe.current.onload = () => {
                this.props.startScene();
            }
        }
    }

    render () {
        return (
           <div ref={this.element} className="mouse-image">
               {
                   this.state.iframe !== null ? <iframe className="iframe" ref={this.iframe} src={this.state.iframe} allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen={true} /> : null
               }
           </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        src: state.imageReducer.imageSrc,
        iframe: state.imageReducer.iframe
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        startScene: () => {
            dispatch( startScene() );
        }
    }
};

const MouseImage = connect(mapStateToProps, mapDispatchToProps)(MouseImageComponent);

export default MouseImage;