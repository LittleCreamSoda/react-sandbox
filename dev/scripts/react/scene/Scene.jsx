import React from 'react';
import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

class Scene extends React.Component {
    element = React.createRef();

    _setUpTHREE = () => {
        const initTHREE = () => {
            this.scene = new THREE.Scene();

            this.camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 1000);
            this.camera.position.set(-250, 100, -40);

            const CONTEXT = this.element.current.getContext('webgl2', { alpha: false}) || this.element.current.getContext('webgl', { alpha: false});
            this.renderer = new THREE.WebGLRenderer({
                context: CONTEXT,
                canvas: this.element.current,
                antialias: this
            });

            this.renderer.setClearColor (0x000000, 1);
            this.renderer.setPixelRatio( window.devicePixelRatio );
            this.renderer.setSize(window.innerWidth, window.innerHeight);
        };

        const setUpControls = () => {
            this.controls = new OrbitControls( this.camera, this.renderer.domElement );
            this.controls.target.set(0, 15, 0);
            this.controls.update();
            this.controls.enableDamping = true;
            this.controls.dampingFactor = 0.1;
        };

        initTHREE();
        setUpControls();
    };

    _setUpScene = () => {
        this.model = {};
        let material;
        let modelTexture;

        const scenes = {
            mic: {
                modelUrl: 'gltf/mic/Tula_white.gltf',
                textureUrl: 'gltf/mic/ambient.jpg'
            },
            spider: {
                modelUrl: 'gltf/spider/scene.gltf',
                textureUrl: 'gltf/spider/textures/Body_Hairs_emissive.png'
            },
            crypt: {
                modelUrl: 'gltf/crypt_location/scene.gltf',
                textureUrl: 'gltf/crypt_location/textures/Body_Hairs_emissive.png'
            }
        };

        const loadModel = () => {
             const loader = new GLTFLoader();
             return new Promise(resolve => {
                 loader.load(scenes.crypt.modelUrl, (gltf) => {
                     this.model.object = gltf.scene;
                     this.model.angle = 0;
                     this.model.x = 0;
                     this.model.y = 0;
                     resolve();
                 })
             });
         };

        const loadTexture = () => {
            const loader = new THREE.TextureLoader();
            return new Promise(resolve => {
                material = new THREE.MeshBasicMaterial({
                    map: loader.load(scenes.spider.textureUrl, (texture) => {
                        modelTexture = texture;
                        resolve();
                    })
                })
            })
        };

        Promise.all([
            loadModel(),
            loadTexture()
        ]).then(() => {
            this.scene.add(this.model.object);
        });
    };

    _setUpLightning = () => {
        const setUpDirectionalLight = () => {
            const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
            directionalLight.name = 'main_light';

            this.lighting.directionalLight = directionalLight;

            this.lighting.directionalLightAngle = 0;
            this.lighting.directionalLightX = 200;
            this.lighting.directionalLightY = -10;
            this.lighting.directionalLightZ = 0;


            directionalLight.position.set(this.lighting.directionalLightX, this.lighting.directionalLightY, this.lighting.directionalLightZ);
            directionalLight.target.position.set(0, 10, 0);

            return directionalLight;
        };

         this.lighting = {};

         const ambientLight  = new THREE.AmbientLight(0xffffff, 1);
         ambientLight.name = 'ambient_light';
         this.scene.add(ambientLight);

         setUpDirectionalLight();

         this.scene.add(this.lighting.directionalLight);
         this.scene.add(this.lighting.directionalLight.target);

         this.lighting.directionalLightHelper = new THREE.DirectionalLightHelper(this.lighting.directionalLight, 10,  0xFFFFFF);
         this.scene.add(this.lighting.directionalLightHelper);
     };

    convertToRadians = (degrees) => {
        return degrees * (Math.PI / 180)
    };

    rotateDirectionalLightPosition = () => {
        this.lighting.directionalLightAngle =  this.convertToRadians(this.lighting.directionalLightAngle);
        this.lighting.directionalLightX = this.lighting.directionalLightX * Math.cos(this.lighting.directionalLightAngle) - this.lighting.directionalLightY * Math.sin(this.lighting.directionalLightAngle);
        this.lighting.directionalLightY = this.lighting.directionalLightY * Math.cos(this.lighting.directionalLightAngle) + this.lighting.directionalLightX * Math.sin(this.lighting.directionalLightAngle);
        // this.lighting.directionalLightZ = this.lighting.directionalLightZ * Math.cos(this.lighting.directionalLightAngle) + this.lighting.directionalLightX * Math.sin(this.lighting.directionalLightAngle);

        this.lighting.directionalLight.position.set(this.lighting.directionalLightX, this.lighting.directionalLightY, this.lighting.directionalLightZ);

        this.lighting.directionalLightAngle += 0.2;
    };

    updateDirectionalLightTarget = () => {
        this.lighting.directionalLight.target.updateMatrixWorld();
        this.lighting.directionalLightHelper.update();
    };

    rotateModel = () => {
        if (this.model.object) this.model.object.rotation.y += 0.006;
        // this.model.angle = this.convertToRadians(this.model.angle);
        // this.model.x = this.model.x * Math.cos(this.model.angle) - this.model.y * Math.sin(this.model.angle);
        // this.model.object.rotation.x += 0.01;
    };

    _animate = () => {
        // this.rotateDirectionalLightPosition();
        this.updateDirectionalLightTarget();
        this.rotateModel();

        this.renderer.render(this.scene, this.camera);

        requestAnimationFrame( this._animate);
    };

    _init = () => {
        this._setUpTHREE();
        this._setUpScene();
        this._setUpLightning();
        this._animate();
    };

    componentDidMount() {
        this._init();
    }

    render() {
        return (
            <canvas ref={this.element} className='scene' />
        )
    }
}



export default Scene;