import React from "react";
import { ThemeContext } from "../theme-provider/ThemeProvider";

class ThemeToggleButton extends React.Component {
    state = {
        currentTheme: this.props.theme
    };

    static contextType = ThemeContext;

    changeTheme = () => {
        this.setState( (state) => {
            let newState = {};
            state.currentTheme === "light" ? newState = { currentTheme: "dark" } : newState = { currentTheme: "light" };
            this.context.changeTheme(newState.currentTheme);
            return newState
        });
    };

    render() {
        const style = this.state.currentTheme === 'light' ? {color: "#000000"} : {color: "#ffffff"};
        return (
                <button onClick={this.changeTheme} style={style}>Сменить тему</button>
        );
    }
}

export default ThemeToggleButton;