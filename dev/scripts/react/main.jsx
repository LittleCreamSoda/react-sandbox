import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';
import store from '../redux/global-store';

import App from './App';

function init () {
    const ROOT = document.querySelector(".app");

    ReactDOM.render(
        <Provider store={store}>
                <App/>
        </Provider>,
        ROOT
    );
}

export default {
    init: init
}