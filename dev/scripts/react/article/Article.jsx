import React from 'react';
import {connect} from "react-redux";
import {changeImage} from "../../redux/action-creators"

import reactImageSrc from 'react.png';
import daMnePohui from "damnepohui.jpg";

class ArticleComponent extends React.Component {
    element = React.createRef();

    componentDidUpdate(prevProps) {
        const iframeSrc = "https://www.youtube.com/embed/UMRNfWSwmPo?autoplay=1";
        this.props.changeImage(daMnePohui, iframeSrc);

        if (this.props.shouldSceneStart) {
            this.element.current.classList.add('star-wars');
        }
    };

    handleMouseLeave = () => {
        this.props.changeImage(reactImageSrc);
    };

    render() {
        return (
            <div ref={this.element} className="article-container" onMouseEnter={this.handleMouseEnter} onMouseLeave={this.handleMouseLeave}>
                {
                    this.props.content ? this.props.content.map( (article, index) => {
                        return <p key={index} className="article">{article}</p>
                    }) : null
                }
            </div>

        );
    }
}

const mapStateToProps = (state) => {
    return {
        content: state.articleReducer.article,
        shouldSceneStart: state.imageReducer.shouldSceneStart
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        changeImage: (src, iframe) => {
            dispatch(changeImage(src, iframe));
        }
    }
};


const Article = connect(mapStateToProps, mapDispatchToProps)(ArticleComponent);

export default Article;
