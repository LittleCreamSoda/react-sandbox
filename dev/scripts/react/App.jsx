import React from 'react';

//react components
import MainContentSection from "./main-content-section/MainContentSection";
import ThemeProvider from "./theme-provider/ThemeProvider";

class App extends React.Component {
    render () {
        return (
            <ThemeProvider>
               <MainContentSection/>
            </ThemeProvider>
        );
    }
}

export default App;