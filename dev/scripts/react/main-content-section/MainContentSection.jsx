import React from "react";


//react components
import Article from '../article/Article';
import Button from '../button/Button';
import MouseImage from "../mouse/MouseImage";
import Scene from "../scene/Scene";
import BackgroundSpace from "../space-background/BackgroundSpace";


class MainContentSection extends React.Component {
    state = {
        mouseData: {
            previous: {
                x: 0,
                y: 0
            },
            position: {
                x: 0,
                y: 0
            },
            direction: {
                x: 0,
                y: 0
            }
        }
    };

    handleMouseMove = (e) => {
        const x = e.pageX + 60;
        const y = e.pageY;

        this.setState( (state) => {
            return {
                mouseData: {
                    previous: {
                        x: state.mouseData.position.x,
                        y: state.mouseData.position.y
                    },
                    position: {
                        x: x,
                        y: y
                    },
                    direction: {
                        x: Math.sign(x - state.mouseData.position.x),
                        y: Math.sign(y - state.mouseData.position.y)
                    }
                }
            }
        });
    };

    render () {
        return (
            <>
                {/*<BackgroundSpace/>*/}
                <Scene/>
                {/*<section className="main-content" onMouseMove={ this.handleMouseMove } >*/}
                {/*    <div className="dummy">*/}
                {/*        <Article/>*/}
                {/*    </div>*/}
                {/*    <Button dataAction={ "./article-response.json" }/>*/}
                {/*    <MouseImage mouseData={ this.state.mouseData }/>*/}
                {/*</section>*/}
            </>

        )
    }
}

export default MainContentSection;