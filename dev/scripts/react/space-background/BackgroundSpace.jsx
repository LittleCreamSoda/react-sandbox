import React from 'react'
import { vec2 } from "gl-matrix";

const RESOLUTION = window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI || 1;

class BackgroundSpace extends React.Component {
    element = React.createRef();

    _createStars = () => {
        let stars = [];
        const starsAmount = 250;

        for (let i = 0; i < starsAmount; i++) {
            const starSize = Math.round(Math.random() * 4) * RESOLUTION;

            const countX = Math.ceil(this.canvas.width / starSize);
            const countY = Math.ceil(this.canvas.height / starSize);

            const x = starSize * Math.round(Math.random() * countX);
            const y = starSize * Math.round(Math.random() * countY);

            stars.push({
                x: x,
                y: y,
                initialX: x,
                initialY: y,
                size: starSize,
                alpha: 1.0,
                deltaSign: -1,
                shiningInterval: Math.floor(Math.random() * 20)
            });
        }

        return stars;
    };

    _createComets = () => {
        let comets = [];
        const cometsAmount = 5;

        for (let i = 0; i < cometsAmount; i++) {
            const cometSize = (Math.random() * 2).toFixed(1) * RESOLUTION;

            const x = Math.sign(Math.random() - 0.5) === 1 ? 0 : Math.random() * this.canvas.width;
            const y = Math.sign(Math.random() - 0.5) === 1 ? 0 : Math.random() * this.canvas.height;

            comets.push({
                x: x,
                y: y,
                velocity: Math.random() * 2,
                size: cometSize,
                directionX: x === 0 ? 1 : -1,
                directionY: y === 0 ? 1 : -1,
            });
        }

        return comets;
    };

    _draw = () => {
        const orbitMovement = (star) => {
            let starCoordinates = vec2.fromValues(star.x, star.y);
            vec2.rotate(starCoordinates, starCoordinates, vec2.fromValues(star.initialX + Math.sign(Math.random() - 0.5) , star.initialY + Math.sign(Math.random() - 0.5)), Math.PI / 150);

            star.x = starCoordinates[0];
            star.y = starCoordinates[1];
        };

        const shift = (object, type) => {
            if (type === "star") {
                let star = object;
                star.x += 0.002 * Math.sign(Math.random() - 0.5);
                star.y += 0.002 * Math.sign(Math.random() - 0.5);
            }
            else if (type === "comet") {
                let comet = object;
                comet.x += comet.velocity * comet.directionX;
                comet.y += comet.velocity * comet.directionY;

                if (comet.x >= this.canvas.width || comet.x < 0 || comet.y >= this.canvas.height || comet.y < 0) {  // генерируем новые координаты
                    comet.x = Math.sign(Math.random() - 0.5) === 1 ? 0 : Math.random() * this.canvas.width;
                    comet.y = Math.sign(Math.random() - 0.5) === 1 ? 0 : Math.random() * this.canvas.height;
                    comet.velocity = Math.random() * 2;
                }
            }
        };

        const shine = (star) => {
            if (star.alpha <= 0.5) {
                star.deltaSign = 1;
            }

            if (star.alpha >= 1) {
                star.deltaSign = -1;
            }

            const deltaAlpha = star.deltaSign * Math.random() * 0.05;
            const alpha = star.alpha + deltaAlpha;

            star.alpha = alpha;

            return alpha;
        };

        const drawStars = (stars) => {
            for (let i = 0; i < stars.length; i++) {
                // orbitMovement(stars[i]);
                shift(stars[i], 'star');

                const starRadius = stars[i].size / 2;

                const alpha = shine(stars[i]);

                this.canvasContext.fillStyle = `rgba(255, 255, 255, ${alpha})`;

                this.canvasContext.beginPath();
                this.canvasContext.arc(
                    (stars[i].x - starRadius),
                    (stars[i].y - starRadius),
                    starRadius,
                    0,
                    2.0 * Math.PI
                );

                this.canvasContext.fill();
                this.canvasContext.closePath();
            }
        };

        const drawComets = (comets) => {
            this.canvasContext.fillStyle = `rgba(255, 255, 255, 255)`;

            for (let i = 0; i < comets.length; i++) {
                shift(comets[i], 'comet');

                const cometRadius = comets[i].size / 2;

                this.canvasContext.beginPath();
                this.canvasContext.arc(
                    (comets[i].x - cometRadius),
                    (comets[i].y - cometRadius),
                    cometRadius,
                    0,
                    2.0 * Math.PI
                );

                this.canvasContext.fill();
                this.canvasContext.closePath();
            }
        };

        const stars = this._createStars();
        const comets = this._createComets();

        this.canvasContext.fillStyle = `rgba(255, 255, 255, 255)`;

        const step = () => {
            setTimeout( () => {
                this.canvasContext.clearRect(0, 0, this.canvas.width, this.canvas.height);
                drawStars(stars);
                drawComets(comets);
                requestAnimationFrame(step)
            }, 1000 / 40);
        };

        step();
    };

    _handleResize = () => {
      this.canvas.width = window.innerWidth;
      this.canvas.height = window.innerHeight;
    };

    _init = () => {
        this.canvas = this.element.current;
        this.canvasContext = this.canvas.getContext('2d');
        this._handleResize();
        this._draw();
    };

    componentDidMount() {
        this._init();
    };

    render() {
        const style = {
            width: "100%",
            height: "100%",
            backgroundColor: "#000000",
            zIndex: "0"
        };

        return (
            <canvas ref={this.element} className="space-background" style={style}/>
        )
    }
}

export default BackgroundSpace;
