# Front-end boilerplate

## Устaновка

Если нет degit:
```
npm i -g degit
```

### Устанавливаем шаблон для webpack:

```
degit https://bitbucket.org/redcollardevs/front-end-boilerplate my-app
cd my-app
yarn
```

### Устанавливаем шаблон для rollup:

```
degit https://bitbucket.org/redcollardevs/front-end-boilerplate#panic my-app
cd my-app
yarn
```

## Сборка

Дев сборка с сервером
```
yarn run dev
```

Прод сборка
```
yarn run prod
```

Тест сборка - без минимизации и с source-maps (толко для webpack)
```
yarn run test
```


